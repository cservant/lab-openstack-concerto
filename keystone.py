from concerto.component import Component
from concerto.dependency import DepType
import time
import logging

from utils import get_host_name_and_ip


class Keystone(Component):

    def create(self):
        self.places = [
            # try merge waiting && initiated
            'started',
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            # try merge waiting && initiated
            'init': ('started', 'initiated', 'deploy', 0, self.initiate),
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'deploy': ('pulled', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['deploy']),
            'mdbd': (DepType.DATA_USE, ['deploy']),
            'kstd': (DepType.DATA_PROVIDE, ['initiated']),
            'keystone': (DepType.PROVIDE, ['deployed'])
        }
        
        self.initial_place = 'started'

    def initiate(self):
        pass

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling keystone start", host, ip)
        time.sleep(1)

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying keystone start", host, ip)
        time.sleep(1)
