import logging

from concerto.component import Component
from concerto.dependency import DepType

import time

from utils import get_host_name_and_ip


class Neutron(Component):

    def create(self):
        self.places = [
            'start',
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            'init': ('start', 'initiated', 'deploy', 0, self.initiate),
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'config': ('initiated', 'pulled', 'deploy', 0, self.config),
            'register': ('initiated', 'pulled', 'deploy', 0, self.register),
            'deploy': ('pulled', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['config']),
            'mdbd': (DepType.DATA_USE, ['config']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'neud': (DepType.DATA_PROVIDE, ['initiated']),
            'neutron': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = "start"

    def initiate(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Initiating Neutron start", host, ip)
        time.sleep(1)

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Neutron start", host, ip)
        time.sleep(1)

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Neutron start", host, ip)
        time.sleep(1)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Neutron start", host, ip)
        time.sleep(1)

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Neutron start", host, ip)
        time.sleep(1)
