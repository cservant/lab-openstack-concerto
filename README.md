# lab-openstack-concerto

## Requirements
* enoslib
* vagrant installed with libvirt
* python 3+
* concerto

## Launch locally
- use the sh file to start python with sudo rights (needed by vagrant)
- after the lab has run, it leaves a **concerto.log** file in /root/ of the main machin (**enos-0**) that is accessible by using 

- ```sudo vagrant ssh enos-0``` && ```sudo su``` to log in as root in the virtual machine and access **/root/concerto.log**