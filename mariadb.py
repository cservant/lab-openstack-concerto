import logging

from concerto.component import Component
from concerto.dependency import DepType
import time

from utils import get_host_name_and_ip


class MariaDB(Component):

    def create(self):
        self.places = [
            'start',
            'initiated',
            'bootstrapped',
            'restarted',
            'deployed'
        ]

        self.transitions = {
            'init': ('start', 'initiated', 'deploy', 0, self.initiate),
            'pull': ('initiated', 'bootstrapped', 'deploy', 0, self.pull),
            'bootstrap': ('initiated', 'bootstrapped', 'deploy', 0, self.bootstrap),
            'restart': ('bootstrapped', 'restarted', 'deploy', 0, self.restart),
            'register': ('restarted', 'deployed', 'deploy', 0, self.register),
            'check': ('restarted', 'deployed', 'deploy', 0, self.check)
        }

        self.dependencies = {
            'common': (DepType.USE, ['register']),
            'haproxy': (DepType.USE, ['check']),
            'mdbd': (DepType.DATA_PROVIDE, ['initiated']),
            'mariadb': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'start'

    def initiate(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Initiating mariadb start", host, ip)
        time.sleep(1)

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling mariadb start", host, ip)
        time.sleep(1)

    def bootstrap(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Bootstrapping mariadb start", host, ip)
        time.sleep(1)

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting mariadb start", host, ip)
        time.sleep(2)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering mariadb start", host, ip)
        time.sleep(3)

    def check(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Checking mariadb start", host, ip)
        time.sleep(1)

