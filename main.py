#!/usr/bin/env python3


from local_deploy import VagrantDeploy

import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Launches the openstack lab')
    parser.add_argument('launch',
                        help='Choose the type of launch, g5k for a launch on grid5000,'
                             ' vagrant for a local launch with vagrant.',
                        choices=['g5k', 'vagrant'])

    args = parser.parse_args()

    if args.launch == 'g5k':
        print('Launching Openstack lab on grid5000')
        # TODO: do the grid5000 launch
        pass
    elif args.launch == 'vagrant':
        print('Launching Openstack lab locally with vagrant')
        vagrant = VagrantDeploy()
        vagrant.provision()

        # don't do this, this destroys things.
        # vagrant.destroy()
