import logging

from concerto.component import Component
from concerto.dependency import DepType
import time

from utils import get_host_name_and_ip


class Nova(Component):

    def create(self):
        self.places = [
            'waiting',
            'initiated',
            'pulled',
            'ready',
            'deployed'
        ]

        self.transitions = {
            'init': ('waiting', 'initiated', 'deploy', 0, self.initiate),
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'config': ('initiated', 'pulled', 'deploy', 0, self.config),
            'create_db': ('initiated', 'pulled', 'deploy', 0, self.create_db),
            'register': ('initiated', 'deployed', 'deploy', 0, self.register),
            'upgrade_db': ('pulled', 'ready', 'deploy', 0, self.upgrade_db),
            'upgrade_api_db': ('pulled', 'ready', 'deploy', 0, self.upgrade_api_db),
            'deploy': ('ready', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['create_db', 'upgrade_db',
                                      'upgrade_api_db']),
            'mdbd': (DepType.DATA_USE, ['create_db', 'upgrade_db',
                                        'upgrade_api_db']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'glad': (DepType.DATA_USE, ['config']),
            'novad': (DepType.DATA_PROVIDE, ['initiated']),
            'nova': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'waiting'

    def initiate(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Initiating Nova start", host, ip)

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Nova start", host, ip)
        time.sleep(1)

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Nova start", host, ip)
        time.sleep(1)

    def create_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Creating Database Nova start", host, ip)
        time.sleep(1.5)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Nova start", host, ip)
        time.sleep(1)

    def upgrade_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading database Nova start", host, ip)
        time.sleep(1)

    def upgrade_api_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading API db Nova start", host, ip)
        time.sleep(1)

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting Nova start", host, ip)
        time.sleep(1)

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Nova start", host, ip)
        time.sleep(1)
