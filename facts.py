import logging
import time

from concerto.component import Component
from concerto.dependency import DepType

from utils import get_host_name_and_ip


class Facts(Component):

    def create(self):
        self.places = [
            # try merge
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'facts_provider': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Facts deployment start", host, ip)
        time.sleep(1)

