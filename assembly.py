#!/usr/bin/env python3

import argparse
import logging

from openstack import OpenStackAssembly

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Launches the openstack assembly')
#    parser.add_argument('inventory',
#                        help='The inventory of hosts and their ips')

    logging.basicConfig(filename='concerto.log',
                        format='%(threadName)s - %(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(message)s',
                        level=logging.DEBUG)
    the_log = logging.getLogger("testing")
    assembly = OpenStackAssembly()
    assembly.set_verbosity(1)
    assembly.deploy()

