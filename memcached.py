import logging

from concerto.component import Component
from concerto.dependency import DepType

import time

from utils import get_host_name_and_ip


class MemCached(Component):
    """ Defines a component for the deployment of Memcached """

    def create(self):
        
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.USE, ['deploy'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying memcached  start", host, ip)
        time.sleep(1)
