import logging

from concerto.component import Component
from concerto.dependency import DepType
import time

from utils import get_host_name_and_ip


class HAProxy(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.USE, ['deploy']),
            'mdbd': (DepType.DATA_USE, ['deploy']),
            'kstd': (DepType.DATA_USE, ['deploy']),
            'novad': (DepType.DATA_USE, ['deploy']),
            'glad': (DepType.DATA_USE, ['deploy']),
            # 'neud': (DepType.DATA_USE, ['deploy']),
            'haproxy': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deployment start", host, ip)
        time.sleep(1)
