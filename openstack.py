import logging

from concerto.assembly import Assembly
from facts import Facts
from memcached import MemCached
from common import Common
from rabbitmq import RabbitMQ
from openvswitch import OpenVSwitch
from haproxy import HAProxy
from mariadb import MariaDB
from keystone import Keystone
from glance import Glance
from neutron import Neutron
from nova import Nova
from utils import get_host_name_and_ip


class OpenStackAssembly(Assembly):
    def __init__(self):
        self.inventory = "inventory.ini"
        self.facts = Facts()
        self.memcached = MemCached()
        self.common = Common(self.inventory)
        self.rabbitmq = RabbitMQ()
        self.openvswitch = OpenVSwitch()
        self.haproxy = HAProxy()
        self.mariadb = MariaDB()
        self.keystone = Keystone()
        self.glance = Glance()
        self.neutron = Neutron()
        self.nova = Nova()
        Assembly.__init__(self)

    def deploy(self):
        self.set_print_time(True)
        # adding the components
        self.add_component("facts", self.facts)
        self.add_component("memcached", self.memcached)
        self.add_component("common", self.common)
        self.add_component("rabbitmq", self.rabbitmq)
        self.add_component("openvswitch", self.openvswitch)
        self.add_component("haproxy", self.haproxy)
        self.add_component("mariadb", self.mariadb)
        self.add_component("keystone", self.keystone)
        self.add_component("glance", self.glance)
        self.add_component("neutron", self.neutron)
        self.add_component("nova", self.nova)

        # connecting components
        self.connect("facts", "facts_provider", "memcached", "facts")
        self.connect("facts", "facts_provider", "common", "facts")
        self.connect("facts", "facts_provider", "rabbitmq", "facts")
        self.connect("facts", "facts_provider", "openvswitch", "facts")
        self.connect("facts", "facts_provider", "haproxy", "facts")

        self.connect("common", "common", "mariadb", "common")

        self.connect("haproxy", "haproxy", "mariadb", "haproxy")
        
        self.connect("mariadb", "mdbd", "haproxy", "mdbd")
        self.connect("mariadb", "mdbd", "keystone", "mdbd")
        self.connect("mariadb", "mdbd", "glance", "mdbd")
        self.connect("mariadb", "mdbd", "neutron", "mdbd")
        self.connect("mariadb", "mdbd", "nova", "mdbd")
        self.connect("mariadb", "mariadb", "keystone", "mariadb")
        self.connect("mariadb", "mariadb", "glance", "mariadb")
        self.connect("mariadb", "mariadb", "neutron", "mariadb")
        self.connect("mariadb", "mariadb", "nova", "mariadb")

        self.connect("keystone", "kstd", "haproxy", "kstd")
        self.connect("keystone", "kstd", "glance", "kstd")
        self.connect("keystone", "kstd", "neutron", "kstd")
        self.connect("keystone", "kstd", "nova", "kstd")
        self.connect("keystone", "keystone", "glance", "keystone")
        self.connect("keystone", "keystone", "neutron", "keystone")
        self.connect("keystone", "keystone", "nova", "keystone")

        self.connect("rabbitmq", "rabd", "glance", "rabd")
        self.connect("rabbitmq", "rabd", "neutron", "rabd")
        self.connect("rabbitmq", "rabd", "nova", "rabd")

        self.connect("glance", "glad", "haproxy", "glad")
        self.connect("glance", "glad", "nova", "glad")

        self.connect("nova", "novad", "haproxy", "novad")

        # adding behaviours to the components
        self.push_b("facts", "deploy")
        self.push_b("memcached", "deploy")
        self.push_b("rabbitmq", "deploy")
        self.push_b("openvswitch", "deploy")       
        self.push_b("common", "deploy")
        self.push_b("haproxy", "deploy")
        self.push_b("mariadb", "deploy")
        self.push_b("keystone", "deploy")
        self.push_b("glance", "deploy")
        self.push_b("neutron", "deploy")
        self.push_b("nova", "deploy")

        # doing things
        self.wait_all()
        self.synchronize()

        # for now it hangs so we will use this to stop the program
        self.terminate()
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Open Stack Assembly Done", host, ip)
        
        # if all is not done for some reason
        self.force_terminate()

