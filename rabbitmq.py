import logging

from concerto.component import Component
from concerto.dependency import DepType

import time

from utils import get_host_name_and_ip


class RabbitMQ(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.USE, ['deploy']),
            'rabbitmq': (DepType.PROVIDE, ['deployed']),
            'rabd': (DepType.DATA_PROVIDE, ['initiated'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying RabbitMQ start", host, ip)
        time.sleep(1)

