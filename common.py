import logging
from subprocess import run

from concerto.component import Component
from concerto.dependency import DepType

from utils import get_host_name_and_ip


class Common(Component):

    def __init__(self, inventory):
        self.playbook = "lab-openstack-concerto/ansible/test.yml"
        self.vars_file = "ansible/group_vars/all.yml"
        self.global_file = ""
        self.pass_file = ""
        self.inventory = "inventory.ini"

        Component.__init__(self)

    def create(self):

        self.places = [
            'initiated',
            'deployed',
            'ktb_deployed'
        ]

        self.transitions = {
            'ktb_deploy': ('initiated', 'ktb_deployed', 'deploy', 0, self.ktb_deploy),
            'deploy': ('ktb_deployed', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'common': (DepType.PROVIDE, ['ktb_deployed', 'deployed']),
            'facts': (DepType.USE, ['ktb_deploy'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : oth deployment of common start", host, ip)
        result = run(["ansible-playbook",
                      self.playbook,
                      "-i", self.inventory,
                      # "-e", "@" + self.vars_file,
                      # "-e", "@" + self.global_file,
                      # "-e", "@" + self.pass_file,
                      #"-e", "action=oth_deploy"]
                     ], capture_output=True)
        logging.debug(result)
        logging.debug(result.returncode)
        return result

    def ktb_deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : ktb deployment of common start", host, ip)
        result = run(["ansible-playbook",
                      self.playbook,
                      "-i", self.inventory,
                      # "-e", "@" + self.vars_file,
                      # "-e", "@" + self.global_file,
                      # "-e", "@" + self.pass_file,
                      #"-e", "action=ktb_deploy"
                     ], capture_output=True)
        logging.debug(result)
        logging.debug(result.returncode)
        return result
