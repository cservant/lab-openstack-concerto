from enoslib.api import discover_networks, generate_inventory, run_command, play_on
from enoslib.infra.enos_vagrant.provider import Enos_vagrant
from enoslib.infra.enos_vagrant.configuration import Configuration

import logging


class VagrantDeploy:
    """This class launches a local deployment with vagrant"""

    def __init__(self):
        self.provider = None

    def provision(self):
        """This function provisions machines for the deployment"""
        logging.basicConfig(level=logging.INFO)

        conf = Configuration() \
            .add_machine(roles=["concerto-node"],
                         flavour="tiny",
                         number=1) \
            .add_machine(roles=["compute", "openstack"],
                         flavour="medium",
                         number=1) \
            .add_machine(roles=["control", "openstack"],
                         flavour="medium",
                         number=1) \
            .add_machine(roles=["network", "openstack"],
                         flavour="medium",
                         number=1) \
            .add_network(roles=["mynetwork"],
                         cidr="192.168.42.0/24") \
            .finalize()

        # claim the resources
        self.provider = Enos_vagrant(conf)
        roles, networks = self.provider.init()

        # generate an inventory.sample compatible with ansible
        discover_networks(roles, networks)

        generate_inventory(roles, networks, "inventory.ini")

        # for the machine with concerto, need to install git and ansible
        with play_on(pattern_hosts="concerto-node", inventory_path="inventory.ini", roles=roles,
                     gather_facts=False) as p:
            # check requirements (for update of python notably and the presence of python)
            p.apt(name=["ansible", "git"], state="present")
            p.apt(upgrade="yes")

            # getting the git repos for concerto and the lab code
            # note down when I need a branch that i need to type with version=branchname
            p.git(repo="https://gitlab.inria.fr/cservant/lab-openstack-concerto.git",
                  dest="/root/lab-openstack-concerto", version="one_playbook")
            p.git(repo="https://gitlab.inria.fr/mchardet/madpp", dest="/root/madpp")
            # copy the host files onto the concerto node
            p.copy(src="inventory.ini", dest="lab-openstack-concerto")
        # generate a ssh key pair on concerto node
        # hangs if key already created
        # todo: add check for key existence
        #  temp fix is to remove manually the key file  or switch key_generation to False
        key_generation = True
        if key_generation:
            cmd_ssh_keygen = 'ssh-keygen -t rsa -b 4096 -C "concerto.key" -N "" -f "concerto"'
            run_command(cmd_ssh_keygen,
                        pattern_hosts="concerto-node",
                        roles=roles,
                        on_error_continue=True
                        )
        # copy pub key to variable
        cmd_get_key = 'cat concerto.pub'
        ssh_key_concerto_node = run_command(cmd_get_key,
                             pattern_hosts="concerto-node",
                             roles=roles,
                             )['ok']['enos-0']['stdout']

        cmd_get_private_key = 'cat concerto'
        ssh_private_key = run_command(cmd_get_private_key,
                             pattern_hosts="concerto-node",
                             roles=roles,
                             )['ok']['enos-0']['stdout']

        # need to upgrade all vms to python 3.7.3
        with play_on(pattern_hosts="all", inventory_path="inventory.ini", roles=roles, gather_facts=False) as pl:
            pl.copy(src="inventory.ini", dest=".")
            pl.apt(name=["build-essential", "zlib1g-dev", "libncurses5-dev", "libgdbm-dev",
                         "libnss3-dev",
                         "libssl-dev", "libreadline-dev", "libffi-dev", "unzip", "wget", "xz-utils"], state="present")
            pl.apt(upgrade="yes")

            # install proper python 3.7.3 version
            pl.get_url(url="https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tar.xz", dest="/root/")
            # using a shell for untar because p.unarchive fails on the archive (disable warning for it
            pl.shell("tar -xf Python-3.7.3.tar.xz", warn=False)
            # building python 3.7.2 skipping optimisation because it's too slow (testing out if it works better
            # without the optimization
            pl.shell("cd Python-3.7.3  && ./configure")
            # doing in two steps for time out ?
            pl.shell("cd Python-3.7.3 && make")
            # we make install to replace the default python binary of the system
            pl.shell("cd Python-3.7.3 &&  make install")
            # we copy the ssh key from the concerto-node to all authorized_keys
            # (technically this copies it to the concerto node who created the key)
            cmd_ssh_copy = 'echo "' + ssh_key_concerto_node + '" >> .ssh/authorized_keys'
            pl.shell(cmd_ssh_copy)
            cmd_ssh_copy2 = 'echo "' + ssh_private_key + '" >> concerto'
            # replace the key file in inventory.ini
            pl.shell('sed -i "s%/home/charlene/lab-concerto-openstack/.vagrant/machines/enos-3/libvirt/private_key%concerto%g" inventory.ini')
            pl.shell('sed -i "s%/home/charlene/lab-concerto-openstack/.vagrant/machines/enos-2/libvirt/private_key%concerto%g" inventory.ini')
            pl.shell('sed -i "s%/home/charlene/lab-concerto-openstack/.vagrant/machines/enos-1/libvirt/private_key%concerto%g" inventory.ini')
            pl.shell('sed -i "s%/home/charlene/lab-concerto-openstack/.vagrant/machines/enos-0/libvirt/private_key%concerto%g" inventory.ini')

        # run the assembly
        cmd_launch_assembly = "export PYTHONPATH=/root/madpp && python3 lab-openstack-concerto/assembly.py"
        result = run_command(cmd_launch_assembly,
                             pattern_hosts="concerto-node",
                             roles=roles,
                             )
        logging.debug(result)
    # print(result['ok']['enos-0']['stdout'])

    # def destroy(self):
    #     """This function destroys the vagrant boxes"""
    #     # destroy the boxes
    #     self.provider.destroy()
